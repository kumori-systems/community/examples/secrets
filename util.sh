#!/bin/bash

# Cluster variables
CLUSTERNAME="..."
REFERENCEDOMAIN="test.kumori.cloud"
CLUSTERCERT="cluster.core/wildcard-test-kumori-cloud"

# Service variables
INBOUNDNAME="secretsinb"
DEPLOYNAME="secretsdep"
DOMAIN="secretsdomain"
SERVICEURL="secrets-${CLUSTERNAME}.${REFERENCEDOMAIN}"
SECRETNAME="mysecret"
SECRETDATA="This a secret message... sssh"

KUMORI_SERVICE_MODEL_VERSION="1.1.6"

# Change if special binaries want to be used
KAM_CMD="kam"
KAM_CTL_CMD="kam ctl"

case $1 in

'refresh-dependencies')
  cd manifests
  # Dependencies are removed and added again just to ensure that the right versions
  # are used.
  # This is not necessary, if the versions do not change!
  ${KAM_CMD} mod dependency --delete kumori.systems/kumori
  ${KAM_CMD} mod dependency kumori.systems/kumori/@${KUMORI_SERVICE_MODEL_VERSION}
  # Just for sanitize: clean the directory containing dependencies
  rm -rf ./cue.mod
  cd ..
  ;;

'create-secret')
  ${KAM_CTL_CMD} register secret $SECRETNAME --from-data "${SECRETDATA}"
  ;;

'create-domain')
  ${KAM_CTL_CMD} register domain $DOMAIN --domain $SERVICEURL
  ;;

'deploy-inbound')
  ${KAM_CTL_CMD} register inbound $INBOUNDNAME \
    --domain $DOMAIN \
    --cert $CLUSTERCERT
  ;;

# To check if our module is correct (from the point of view of the CUE syntax and
# the Kumori service model), we can use "kam process" to enerate the JSON
# representation
'dry-run')
  ${KAM_CMD} process deployment -t ./manifests
  ;;

'deploy-service')
  ${KAM_CMD} service deploy -d deployment -t ./manifests $DEPLOYNAME -- \
    --comment "Secrets service" \
    --wait 5m
  ;;

'link')
  ${KAM_CTL_CMD} link $DEPLOYNAME:restapi $INBOUNDNAME:inbound
  ;;

'deploy-all')
  $0 create-secret
  $0 create-domain
  $0 deploy-inbound
  $0 deploy-service
  $0 link
  ;;

'describe')
  ${KAM_CMD} service describe $DEPLOYNAME
  echo
  echo
  ${KAM_CMD} service describe $INBOUNDNAME
  ;;

# Test the secrets service
'test')
  echo "Test secret stored in env variable: (expected This a secret message... sssh)"
  curl https://${SERVICEURL}/secretenv; echo
  echo "Test secret stored in file: (expected This a secret message... sssh)"
  curl https://${SERVICEURL}/secretfile; echo
  ;;

'unlink')
  ${KAM_CTL_CMD} unlink $DEPLOYNAME:restapi $INBOUNDNAME:inbound
  ;;

'undeploy-service')
  ${KAM_CMD} service undeploy $DEPLOYNAME -- --wait 5m
  ;;

'undeploy-inbound')
  ${KAM_CMD} service undeploy $INBOUNDNAME -- --wait 5m
  ;;

'delete-domain')
  ${KAM_CTL_CMD} unregister domain $DOMAIN
  ;;

'delete-secret')
  ${KAM_CTL_CMD} unregister secret $SECRETNAME
  ;;

'undeploy-all')
  $0 unlink
  $0 undeploy-service
  $0 undeploy-inbound
  $0 delete-domain
  $0 delete-secret
  ;;

*)
  echo "This script doesn't contain that command"
	;;

esac
