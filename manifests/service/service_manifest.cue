package service

import (
  k "kumori.systems/kumori:kumori"
  f ".../components/frontend:component"
)

#Artifact: {
  ref: name:  "service"

  description: {

    //
    // Kumori Component roles and configuration
    //

    config: {
      resource: {
        appsecret: k.#Secret
      }
    }

    role: {
      frontend: artifact: f.#Artifact
    }

    role: {
      frontend: {
        config: {
          resource: {
            appsecret: description.config.resource.appsecret
          }
        }
      }
    }

    //
    // Kumori Service topology: how roles are interconnected
    //

    srv: {
      server: {
        restapi: { protocol: "http", port: 80 }
      }
    }

    connect: {
      // Outside -> FrontEnd (LB connector)
      inbound: {
        as: "lb"
  			from: self: "restapi"
        to: frontend: "restapi": _
      }
    }
  }
}
