package deployment

import s ".../service:service"

#Deployment: {
  name: "secretsdep"
  artifact: s.#Artifact
  config: {
    resource: {
      appsecret: secret: "mysecret"
    }
    scale: detail: frontend: hsize: 1
    resilience: 0
  }
}
