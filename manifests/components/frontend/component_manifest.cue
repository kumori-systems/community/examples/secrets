package component

import k "kumori.systems/kumori:kumori"

#Artifact: {
  ref: name:  "components/frontend"

  description: {

    srv: {
      server: {
        restapi: { protocol: "http", port: 8080 }
      }
    }

    config: {
      resource: {
        appsecret: k.#Secret
      }
    }

    size: {
      bandwidth: { size: 10, unit: "M" }
    }

    probe: frontend: {
      liveness: {
        protocol: http : { port: srv.server.restapi.port, path: "/health" }
        startupGraceWindow: { unit: "ms", duration: 30000, probe: true }
        frequency: "medium"
        timeout: 30000  // msec
      }
      readiness: {
        protocol: http : { port: srv.server.restapi.port, path: "/health" }
        frequency: "medium"
        timeout: 30000 // msec
      }
    }

    code: {
      frontend: {
        name: "frontend"
        image: {
          hub: { name: "", secret: "" }
          tag: "kumoripublic/examples-secrets-frontend:v1.0.6"
        }
        mapping: {
          filesystem: {
            "/kumori/secret.json": {
              data: secret: "appsecret"
            }
          }
          env: {
            HTTP_SERVER_PORT_ENV: value: "\(srv.server.restapi.port)"
            KUMORI_SECRET: secret: "appsecret"
          }
        }
        size: {
          memory: { size: 100, unit: "M" }
          mincpu: 100
          cpu: { size: 200, unit: "m" }
        }
      }
    }
  }
}
